## HW
FPGA I/O: where: voltage
* data (standard pmod connector JE): bottom left side of board, just plug sensor in to top or bottom row of pins: 
* power: in PMOD, pin 6 and 12 (left side top and bottom): up to 1A of current
* ground: in PMOD, pin 5 and 11 (2nd from left, top and bottom): up to 1A of current
* slide switches: : 10k? VCC3V3 
* leds: : 330? VCC3V3
* press button: : 10k? VCC3V3

## Coding challenges
as fast as possible, try not to google for code.

## NOTES
discussion of code here: https://gitlab.eurecom.fr/renaud.pacalet/ds-2017/tree/master#sessions20170413

dht11 as a team, so subdir of dht11 should be any name you like! Read brief info about VHDL for next challenge. std_uunisgned are better for math on vectors. also ints and reals - when u use int you can specify its range, dont have to use from lowest to highest, can reduce range using this expression. defining subtype of int type that is limited to this range. if you want to do conversions between these different types, first sit: types are strongly correlated (real, ints are strongly) to convert from one strongly correlated type to another, use name of target type as a conversion function. so 
signal i: integer range 0 to 250;
i <= int(r); converts real r into closests int and assigns to r
r <= real(i): converts i into real and assigns to r.
std_ulogic_vector and u_unsigned are strongly correlated. 
integer and std_ulogic vector are not strongly correlated, very different - one is numbers and other is array. numeric_std package provides functions to do the job - to conver ta u_unsigned vec to an int, use to_integer function, considering in unsigned form, always + or 0. different if u is u_signed, in this case bin repzn is considered as 2s complement and result could be neg int.
u <=to_unsigned(i,0); takes 2 parms: int to convert and # of bits for expected results. instead of number of bits, can pass as second param the destination vector. 
to convert between types, have a look at this main page!
to compare int and vector types, most commonly used types, and want to represent a given data, can use int range 0 to 250 or std_ulogic_vector(7 downto 0), or u_unsigned (7 downto 0). which is the best? it depends! if you want to do only math, then ints are the best.  mixture of bitwise and arith = u_unsigned. 
2 impt subtletiems - when u declare an int here, signal of type 0 and 255, if value of i goes beyond range -1 or > 255, there is error. "at lne x in foo.vhd, you tried to assign and it's forbidden" powerful way to debug. if you know that this signal is limted in range, DECLARE it, dont just use int. put as much knowledge as you have about design in yr declarations.
to do math on u_unsigned, can add ints to u_unsigned or add other u_unsigned to u_unsigned. auto wrapping, no overflow. if current vector is all 1s, and you add1, what you get is 0.  wraps automatically, no overflow. strong diff with int. sometimes you want cell wrapping, but to know if yr out of range use integer! 
2 - not just for simulatin, also for synth. types are all synthesiable. matter is how synth will handle them. if u use tthis declaration, synth will use 8 bits. xform into into 8 bit signal. if you don't specify, 32 bits. makes huge dif on size and space of design. to know i is declared as int, but range is 0-255, will put much bigger, be much slower. if you know that int will always be limited to a range, SPECIFY IT.
ALSO instead of 0 to 255, could have 0 to 249. synth will also reserve 8 bits, which are needed, but during simulation you can detect an out of range of 250 which you can't do witha  u_unsigned on 8 bits. can be even mor ea bit accurate than number of bits needed but also get exact range u want to control. 
parametizable. still get adder - 
generic declaration, similar to a port declaration - in entity you can add generic declaration with name type opt default value. now size can be used as constant for whole description. in port, eg, can declare port b as 1 to size (see eg).
can use same size constant in arch that describes ... at instantiation time. in begin block, when instantiating sub circuit entity, can port mpa local signals OR map new value to size generic parameter. instantiating subcircuit not with default size, 16, but with size 37. USE generics to make designs more reusable.
LAST THING; random number generation: in vhdl 2008, map real package of IEEE contains a procedure called UNIFORM, taekes 2 positive seeds, mode inout means that every time proc is called they are read and modified, and 3rd param x mode out of type real. every time u call, assigns to x another pseudorandom value between 0 and 1. 
HERE: in session 201704, look at this! copy paste! use generics, uniforms, etc. 

