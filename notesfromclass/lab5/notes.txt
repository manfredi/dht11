Types we used up til now are bit and int.

bit - enumerated type: definition of type bit is the list of all its values. 
type bit is ('0', '1'); -- this is the definition. The order here is important, because if you decalre a var of type bit and don't assign a value, it takes the leftmost thing.

variable b: bit; -- at beginning, b will take '0', you can assign:

variable b: bit = '1'; -- careful, not synthesizable. 

you can define your own enumerated types if you wish, a good example are the states of a state machine: idle/waiting/running. 

type states_type is (IDLE, WAITING, RUNNING);

that is synthesizable. could also put numbers if it's not an enum type but an int from 0-2, 0 is idle, waiting, running, so could do:

variable state :  states_type

state: natural range 0 to 2;
natural is range of ints, from 0 to largest int. 

numeric or enumerated. 

If we do enumerated, then synthesizer is free to choose representation. These names don't force the synth to assign a given number of bits to represent the states and a given combo of values for the states. It is free to select the best - it will do this, and apply several optimization strategies and select best possible encoding of the states.

Also it's meaningful to read enumerated types. 

Drawback of enum types: if you look at result of synth and ask the synther to produce VHDL or Verilog ro anthing after synthesis, there will be a mismatch bw simulation before and after synthesis. after synthesis, will see groups of bits. better to use enum types of your own to define states of a state machine.

Bit, states_type are both enum types.

You will see a table in the synth report that tells you encoding of enum types, but after synth there is not race of initial states. 

Bit represents one single, 2 values of info. Frequently in hw designw e group bits in packets to represent state numbers or states or something else. This grouping can be erased.

type bit_vector is array(natural range < >) of bits
natural range <> - unconstrained type declaration. all we say is that bit vector is 1d array of bits and index is of type natural. dont specify branch, can even be empty. 

after bit_vector declaration, can do

variable v : bit_vector(17 to 35) -- here, bc hw needs hw and phys rpzn of this, must specify what is the length. can be 17 to 35. now knows leftmost and rightmost.

(35 downto 16) -- label leftmost higher tha right most. leftmost is MSB, rightmost is LSB. Leftmost is n-1, rightmost is 0. usually.

We can define vectors of existing bits. Could define a vector of ints, bools, etc. 

Vectors are useful to rpznt numbers, if you only want 8 bit numbers. if you use ints, synth uses 32 bits. use vectors to reduce number of bits. or opposite - if you want 1024 bits, use vectors. Also, if you want to do math but also address individual bits of bin rpzn of number, than use vectors - easy to find MSB and LSB in vectors or any bit in between. Vectors are common.

Problem: bit vector is std type, declared exactly as that in std_standard. you don't have to declare std_standard or use of it bc its automatic. bit_vector already declared, but problem: cant do any arithmetic with it. 

ieee.numeric_bit -- declares 2 types: 1) unsigned 2) signed these 2 types have exactly same declaration as bit vector, also 1d arrays of bits indexed with a natural #. overloads arith operators so you can perform arith ops. can add 2 unsigned, or unsighed + int. 

but bit with 2 values not always sufficient. last time saw

std_ulogic - 9 values, 'u', 'z'.... this is decalred in:
ieee.std_logic_1154

also, in same package: std_ulogic_vector but can't do arith on it, so 3rd package:

ieee.numeric_std;

does same as numeric_bit but instead of bits it does it on std_ulogic. 2 types, unsigned and signed, with same definition as std_ulogic vector on which you can perform arith ops. actually signed/unsigned are same as std_logic, not ulogic. unfortunate - means that signed/unsigned are resolved. no error if you accidentally drive 2 at once. so in vhdl 2000, 2 more types:

unresolved_unsigned, alias u_unsigned
unresolved_signed, alias u_signed. same definition as std_ulogic vector and can do math on them.

if you can work in vhdl 2008, prefer u_unsigned, u_signed bc likely you want to do multiple drive on vecs on whcih you also do arith.

last note:
Vivado, synth tool we use for zynq, doesn't know u_unsigned or u_signed. 

advice for DHT11: first work with u_unsigned, u_signed and at end, after simulating and validating, replace all u_unsigned/signed by unsigned/signed, just for synthesis. or create alias.

note: in industry, not always best practices. they always use std_logic, dont know other types exist. stupid: accidental short circuits. because std_ulogic nonresovled types generates warnings and errors. bad practice.

freerange vhdl also does std_logic, not ulogic.  


