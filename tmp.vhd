-- vim: set textwidth=0:

use std.env.all; -- to use --stop and finish

library ieee; -- to use ieee.std_logic_1164 package
use ieee.std_logic_1164.all; -- to use std_ulogic and std_ulogic_vector types
use ieee.numeric_std.all;
use work.dht11_pkg.all;
use work.dht11_sim_pkg.all;

-- Simulation environments are frequently back boxes
entity sa_sim is
	generic(
                freq:    positive range 1 to 1000 := 2  -- Clock frequency (MHz)
        );
end entity sa_sim;

architecture sim of sa_sim is
                
		--signal freq:    positive range 1 to 1000; --:= 2; -- Clock frequency (MHz)
               signal clk:       std_ulogic;
                signal rst:        std_ulogic; -- Active high synchronous reset
               signal  btn:        std_ulogic;
                signal sw:         std_ulogic_vector(3 downto 0); -- Slide switches
                signal data_in:    std_ulogic;
                signal data_drv:  std_ulogic;
                signal led:       std_ulogic_vector(3 downto 0); -- LEDs

       

begin

    -- entity instantiation of the Design Under Test
  

    dut: entity work.dht11_sa(rtl)
	generic map(
                freq => freq
        );
        port map(
            clk     => clk,
            rst => rst
            btn => btn,
			sw => sw,
			data_in => data_in,
			data_drv => data_drv,	
        	led => led
		);



    -- clock generator
    process
    begin
        clk <= '0';
        wait for 10 ns;
        clk <= '1';
        wait for 10 ns;
    end process;

    process
    begin
        wait until rst = '1' for 1 us;

        count <= count + 1;

        if (timer_rst = '1') then
            wait until rising_edge(clk);
            count <= 0;
        end if;
    end process;

    
    -- 01011 
    process
    begin
        wait until count = dht11_reset_to_start_min; --1sec
        btn <= '1';
        wait until count = dht11_start_duration_min +1000 ; --20ms
        wait until count = dht11_start_to_ack_min; --20us
        
        wait until rising_edge(clk);
        data_in <= '0';
        wait until rising_edge(clk);
        --fall <= '0';
        
        wait for dht11_ack_duration_t; --80us
     
        
        wait until rising_edge(clk);
        data_in <= '1';
        wait until rising_edge(clk);
        --rise <= '0';
        wait for dht11_ack_to_bit_t; --80

        wait until rising_edge(clk);
        data_in <= '0';
        wait until rising_edge(clk);
        --fall <= '0';
        wait for dht11_bit_duration_t; --50us
        
        
        wait until rising_edge(clk);
        data_in <= '1';
        wait until rising_edge(clk);
       -- rise <= '0';
        wait for dht11_bit0_to_next_min_t ;--26 us
    
	-- data starts here   
        for i in 39 downto 9 loop

		wait until rising_edge(clk);
		data_in <= '0';
		wait until rising_edge(clk);
		--fall <= '0';
		wait for dht11_bit_duration_t; --50us
		
		
		wait until rising_edge(clk);
		data_in <= '1';
		wait until rising_edge(clk);
		--rise <= '0';
		wait for dht11_bit0_to_next_min_t ; --26 us

	end loop; -- 

        wait until rising_edge(clk);
        data_in <= '0';
        wait until rising_edge(clk);
        --fall <= '0';
        wait for dht11_bit_duration_t;
        
        wait until rising_edge(clk);
        data_in <= '1';
        wait until rising_edge(clk);
        --rise <= '0';
        wait for dht11_bit1_to_next_t ;

	-- checksum starts here
	for i in 7 downto 1 loop

		wait until rising_edge(clk);
		data_in <= '0';
		wait until rising_edge(clk);
		--fall <= '0';
		wait for dht11_bit_duration_t; --50us
		
		
		wait until rising_edge(clk);
		data_in <= '1';
		wait until rising_edge(clk);
		--rise <= '0';
		wait for dht11_bit0_to_next_min_t ; --26 us
	end loop;
        
	wait until rising_edge(clk);
        data_in <= '0';
        wait until rising_edge(clk);
        --fall <= '0';
        wait for dht11_bit_duration_t;
        
        wait until rising_edge(clk);
        data_in <= '1';
        wait until rising_edge(clk);
        --rise <= '0';
        wait for dht11_bit1_to_next_t ;


    end process;

end architecture sim;

